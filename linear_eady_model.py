"""
Solve the linear Eady equations by iterative method
by subclassing LinearSlice and providing different
residuals.
"""

from slicemodels import LinearSlice
from firedrake import *
import sys

class EadyLinearSlice(LinearSlice):

    def __init__(self,L,H,nlayers,ncolumns,pvertical,phorizontal,
                 dt,nits,alpha,Nsq,csq,f,dbdy,
                 u0_exp,v0_exp,p0_exp,b0_exp):
        self.dbdy = dbdy
        LinearSlice.__init__(self,L,H,nlayers,ncolumns,pvertical,phorizontal,
                             dt,nits,alpha,Nsq,csq,f,
                             u0_exp,v0_exp,p0_exp,b0_exp)

    def SetupInitialP(self):
        ##Calculate hydrostatic pressure
        # get initial b
        oldb = self.oldb

        # get F
        v = TrialFunction(self.V1v)
        w = TestFunction(self.V1v)

        bcs = [DirichletBC(self.V1v, Expression(("0.", "0.")), "bottom")]

        a = inner(w,v)*dx
        L = w[1]*oldb*dx
        F = Function(self.V1v)

        solve(a == L, F, bcs=bcs)

        # define mixed function space
        WV = (self.V1v)*(self.V2)
        WT = (self.Vt)*(self.V2)

        # get pprime
        v,pprime = TrialFunctions(WV)
        w,phi = TestFunctions(WV)

        bcs = [DirichletBC(WV[0], Expression(("0.", "0.")), "bottom")]

        a = (
            inner(w,v) + div(w)*pprime + div(v)*phi
            )*dx
        L = phi*div(F)*dx
        w1 = Function(WV)

        solve(a == L, w1, bcs=bcs)
        v,pprime = w1.split()

        ##Boundary correction
        #get phat
        ptrial,q = TrialFunctions(WT)
        gamma,phi = TestFunctions(WT)

        bcs = [DirichletBC(WT[0], Expression("0."), "bottom")]

        a = (gamma*ptrial+gamma.dx(1)*q+phi*ptrial.dx(1))*dx
        L = phi*pprime*dx

        F = Function(WT)
        solve(a == L, F, bcs=bcs)

        phat,q = F.split()


        # get pbar
        v,pbar = TrialFunctions(WV)
        w,phi = TestFunctions(WV)
        
        bcs = [DirichletBC(WV[0], Expression(("0.","0.")), "bottom")]

        a = (
            inner(w,v) + div(w)*pbar + div(v)*phi
            )*dx
        L = w[1]*phat*ds_t
        w1 = Function(WV)

        solve(a == L, w1, bcs=bcs)
        v,pbar = w1.split()


        # get corrected pressure
        self.oldp.assign(project(pprime,self.V2) - project(pbar,self.V2)/self.H) 


    def SetupInitialV(self):
        ##Calculate blanced velocity
        oldu, oldv = self.oldvel.split()

        #get pressure gradient
        g = TrialFunction(self.V1)
        wg = TestFunction(self.V1)

        bcs = [DirichletBC(self.V1, Expression(("0." , "0.")), x)
               for x in ["top", "bottom"]]

        a = inner(wg,g)*dx
        L = -div(wg)*self.oldp*dx
        pgrad = Function(self.V1)
        solve(a == L, pgrad, bcs=bcs)

        #get oldv
        phi = TestFunction(self.V2)
        m = TrialFunction(self.V2)

        a = self.f*phi*m*dx
        L = phi*pgrad[0]*dx
        solve(a == L, oldv)


        ##Solve the blanced equation
        #psi is a stream function
        xsi = TestFunction(self.V0)
        psi = TrialFunction(self.V0)

        Forcing = Function(self.V0).interpolate(Expression(("x[1]-H/2"),H=self.H))

        Equ = (
            xsi.dx(0)*self.Nsq*psi.dx(0) + xsi.dx(1)*self.f**2*psi.dx(1)
            + self.dbdy*xsi.dx(0)*oldv - self.dbdy*xsi.dx(1)*self.f*Forcing
            )*dx

        Au = lhs(Equ)
        Lu = rhs(Equ)
        stm = Function(self.V0)
        solve(Au == Lu, stm)

        # get oldu
        utrial = TrialFunction(self.V1)
        w = TestFunction(self.V1)

        a = inner(w,utrial)*dx
        L = (w[0]*(-stm.dx(1))+w[1]*(stm.dx(0)))*dx
        solve(a == L, oldu)


    def SetupVResidual(self):
        # get parameters
        dt = self.dt
        alpha = self.alpha
        f = self.f
        dbdy = self.dbdy

        # get functions
        newu, newv = self.newvel.split()
        oldu, oldv = self.oldvel.split()
        # get space
        V2 = self.V2
        # set up ubar etc
        ubar = (1-alpha)*oldu + alpha*newu
        vbar = (1-alpha)*oldv + alpha*newv

        #Expression for Forcing

        Forcing = Function(self.V0).interpolate(Expression(("x[1]-H/2"),H=self.H))

        # trial and test functions
        dv = TrialFunction(V2)
        wv = TestFunction(V2)
        self.vres = Function(V2)
        self.vmass = inner(wv,dv)*dx
        self.Lvres = (wv*(newv-oldv) + dt*f*wv*ubar[0]
                      + dt*wv*dbdy*Forcing)*dx

        vresproblem = LinearVariationalProblem(self.vmass, self.Lvres, self.vres)
        self.vressolver = LinearVariationalSolver(vresproblem, 
                                             parameters={'ksp_type': 'cg'})


    def SetupBResidual(self):
        # get parameters
        dt = self.dt
        alpha = self.alpha
        Nsq = self.Nsq
        dbdy = self.dbdy
        # get functions
        newu, newv = self.newvel.split()
        oldu, oldv = self.oldvel.split()
        newb = self.newb
        oldb = self.oldb
        # get space
        Vt = self.Vt
        # set up ubar etc
        ubar = (1-alpha)*oldu + alpha*newu
        vbar = (1-alpha)*oldv + alpha*newv
        # trial and test functions
        db = TrialFunction(Vt)
        gamma = TestFunction(Vt)

        self.bmass = gamma*db*dx
        self.Lbres = (
            gamma*(newb-oldb + dt*Nsq*ubar[1]
                   + dt*dbdy*vbar)
            )*dx
        self.bres = Function(Vt)

        bresproblem = LinearVariationalProblem(self.bmass, self.Lbres, 
                                               self.bres)
        self.bressolver = LinearVariationalSolver(bresproblem, 
                                             parameters={'ksp_type': 'cg'})


    def SetupDumpspace(self):
        Sdump = self.Sdump
        if(Sdump == 'V0'):
            dumpspace = self.V0
        elif(Sdump == 'V1'):
            dumpspace = self.V1
        elif(Sdump == 'V2'):
            dumpspace = self.V2
        elif(Sdump == 'Vt'):
            dumpspace = self.Vt
        elif(Sdump == 'W'):
            dumpspace = self.W
        else:
            print "Error in SetupProjectResult: Sdump cannot be", Sdump+". abort"
            sys.exit()

        return dumpspace


    def dump(self):
        # get parameters
        dt = self.dt
        Tdump = self.Tdump

        ## specify which result to dump
        self.input_b = self.oldb
        self.input_p = self.oldp

        # dump result every Tdump
        if (self.dumpt == 0.):
            dumpspace = self.SetupDumpspace()

            self.file_b = File(self.filename+'_b'+'.pvd')
            self.output_b = Function(dumpspace)
            self.prsolver_b = self.SetupPrSolver(self.input_b,self.output_b,dumpspace)
            self.prsolver_b.solve()
            self.file_b << self.output_b

            self.file_p = File(self.filename+'_p'+'.pvd')
            self.output_p = Function(dumpspace)
            self.prsolver_p = self.SetupPrSolver(self.input_p,self.output_p,dumpspace)
            self.prsolver_p.solve()
            self.file_p << self.output_p

        elif(self.dumpt > Tdump - 0.5*dt):
            self.dumpt -= Tdump

            self.prsolver_b.solve()
            self.file_b << self.output_b

            self.prsolver_p.solve()
            self.file_p << self.output_p


    def solve(self,t,end,Tdump,filename,Sdump):

        import numpy as np
        
        # get parameters
        dt = self.dt
        nits = self.nits

        # set parameters
        self.Tdump = Tdump
        self.Sdump = Sdump
        self.filename = filename

        # balanced initialization
        self.SetupInitialP()
        self.SetupInitialV()
        
        # setup residuals
        # Subclasses may redefine these functions
        self.SetupBResidual()
        self.SetupPResidual()
        self.SetupUResidual()
        self.SetupVResidual()

        # setup solvers, these are always the same
        self.SetupUSolver()
        self.SetupDpSolver()
        self.SetupDbSolver()
        
        # set newvel,newp,newb
        self.newvel.assign(self.oldvel)
        self.newp.assign(self.oldp)
        self.newb.assign(self.oldb)

        # open file
        self.dumpt = 0.
        self.dump()

        # time loop
        print "Starting the time loop!"

        while (t <= end):
            for its in range(nits):
                self.GetBResidual()
                self.GetPResidual()
                self.GetUResidual()
                self.GetVResidual()

                #This code is always the same
                self.usolver.solve()
                self.dpsolver.solve()
                self.dbsolver.solve()
                    
                self.newvel += self.Deltavel
                self.newp += self.Deltap
                self.newb += self.Deltab
                    
            self.oldvel.assign(self.newvel)
            self.oldp.assign(self.newp)
            self.oldb.assign(self.newb)
                    
            t += dt
            self.dumpt += dt
            print t, end, self.dumpt, self.Tdump, self.Sdump

            self.dump()


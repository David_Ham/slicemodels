from slicemodels import LinearSlice
from firedrake import *

u0_exp = Expression(("0.0","0.0"))
v0_exp = Expression("0.0")
p0_exp = Expression("0.01*10000*cos(pi*x[1]/10000.)/(1+pow(x[0]-3000000.,2)/pow(100000.,2))")
b0_exp = Expression("0.01*sin(pi*x[1]/10000.)/(1+pow(x[0]-3000000.,2)/pow(100000.,2))")

dt = 100.
Model = LinearSlice(L=6000000.,H=10000.,nlayers=50,ncolumns=50,pvertical=1,phorizontal=1,
                 dt=dt,nits=2,alpha=0.5,Nsq=1.e-4,csq=300.**2,f=1.0e-4,
                 u0_exp=u0_exp,v0_exp=v0_exp,p0_exp=p0_exp,b0_exp=b0_exp)

Model.solve(t=0.0,end=600*dt,Tdump=10*dt,filename='test_linear')

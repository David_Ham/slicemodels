from slicemodels import LinearSlice
from firedrake import *


class MeanFlowLinearSlice(LinearSlice):

    def __init__(self,L,H,nlayers,ncolumns,pvertical,phorizontal,
                 dt,nits,alpha,Nsq,csq,f,
                 u0_exp,v0_exp,p0_exp,b0_exp,U):
        self.U = U
        LinearSlice.__init__(self,L,H,nlayers,ncolumns,pvertical,phorizontal,
                 dt,nits,alpha,Nsq,csq,f,
                 u0_exp,v0_exp,p0_exp,b0_exp)
        # Vorticity space
        Vz_elt = OuterProductElement(self.IUh0,self.IUv0)
        Vz = FunctionSpace(self.mesh,Vz_elt)
        self.Vz = Vz
        self.oldz = Function(Vz)
        self.newz = Function(Vz)
        # Mesh-related functions
        n = FacetNormal(self.mesh)
        # ( dot(v, n) + |dot(v, n)| )/2.0
        uadv = Function(self.V1).project(Expression(('20.','0.')))
        self.un = 0.5*(dot(uadv, n) + abs(dot(uadv, n)))
        self.vfile = File('vort.pvd')

    def GetVorticity(self):
        # get parameters
        Vz = self.Vz
        oldu, oldv = self.oldvel.split()
        # trial and test functions
        z = TrialFunction(Vz)
        sigma = TestFunction(Vz)
        az = sigma*z*dx
        Lz = dot(curl(sigma),oldu)*dx
        zetaproblem = LinearVariationalProblem(az, Lz, self.oldz)
        self.zetasolver = LinearVariationalSolver(zetaproblem, 
                                             parameters={'ksp_type': 'cg'})

    def VorticityAdvection(self):
        dt = self.dt
        alpha = self.alpha
        U = self.U
        oldb = self.oldb
        newb = self.newb
        bbar = (1-alpha)*oldb + alpha*newb
        oldz = self.oldz
        # get space
        Vz = self.Vz
        # trial and test functions
        z = TrialFunction(Vz)
        sigma = TestFunction(Vz)
        zbar = alpha*z + (1-alpha)*oldz
        Ezadv = (sigma*(z-oldz) - dt*sigma.dx(0)*(U*zbar + bbar))*dx
        azadv = lhs(Ezadv)
        Lzadv = rhs(Ezadv)
        zadvproblem = LinearVariationalProblem(azadv, Lzadv, self.newz)
        self.zadvsolver = LinearVariationalSolver(zadvproblem, 
                                             parameters={'ksp_type': 'cg'})

    def SetupUResidual(self):
        # get parameters
        dt = self.dt
        alpha = self.alpha
        U = self.U
        # get functions
        newu, newv = self.newvel.split()
        oldu, oldv = self.oldvel.split()
        newp = self.newp
        oldp = self.oldp
        newb = self.newb
        oldb = self.oldb
        newz = self.newz
        oldz = self.oldz
        # get space
        V1 = self.V1
        # set up ubar etc
        ubar = (1-alpha)*oldu + alpha*newu
        pbar = (1-alpha)*oldp + alpha*newp
        bbar = (1-alpha)*oldb + alpha*newb
        zbar = (1-alpha)*oldz + alpha*newz
        # trial and test functions
        du = TrialFunction(V1)
        w = TestFunction(V1)
        self.GetVorticity()
        self.VorticityAdvection()

        # set velocity residual
        self.ures = Function(V1)
        self.umass = inner(w,du)*dx
        self.Lures = (
            inner(w,newu-oldu) - dt*U*w[1]*zbar
            - dt*div(w)*(pbar + U*ubar[0])
            - dt*w[1]*bbar
            )*dx

        uresproblem = LinearVariationalProblem(self.umass, self.Lures, self.ures)
        self.uressolver = LinearVariationalSolver(uresproblem, 
                                             parameters={'ksp_type': 'cg'})


    def SetupPResidual(self):
        # get parameters
        dt = self.dt
        alpha = self.alpha
        csq = self.csq
        U = self.U
        # get functions
        newu, newv = self.newvel.split()
        oldu, oldv = self.oldvel.split()
        newp = self.newp
        oldp = self.oldp
        un = self.un
        # get space
        V2 = self.V2
        # set up ubar etc
        ubar = (1-alpha)*oldu + alpha*newu
        pbar = (1-alpha)*oldp + alpha*newp
        # trial and test functions
        dp = TrialFunction(V2)
        phi = TestFunction(V2)

        self.pmass = phi*dp*dx
        self.Lpres = (phi*(newp-oldp + dt*csq*div(ubar)) - dt*U*(phi.dx(0)*pbar))*dx + dt*dot(jump(phi),(un('+')*pbar('+') - un('-')*pbar('-')))*dS_v
        self.pres = Function(V2)

        presproblem = LinearVariationalProblem(self.pmass, self.Lpres, 
                                               self.pres)
        self.pressolver = LinearVariationalSolver(presproblem, 
                                             parameters={'ksp_type': 'cg'})

    def SetupBResidual(self):
        # get parameters
        dt = self.dt
        alpha = self.alpha
        Nsq = self.Nsq
        U = self.U
        # get functions
        newu, newv = self.newvel.split()
        oldu, oldv = self.oldvel.split()
        newb = self.newb
        oldb = self.oldb
        un = self.un
        # get space
        Vt = self.Vt
        # set up ubar etc
        ubar = (1-alpha)*oldu + alpha*newu
        bbar = (1-alpha)*oldb + alpha*newb
        # trial and test functions
        db = TrialFunction(Vt)
        gamma = TestFunction(Vt)

        self.bmass = gamma*db*dx
        self.Lbres = (gamma*(newb-oldb + dt*Nsq*ubar[1]) - dt*U*gamma.dx(0)*bbar)*dx + dt*dot(jump(gamma),(un('+')*bbar('+') - un('-')*bbar('-')))*dS_v
            
        self.bres = Function(Vt)

        bresproblem = LinearVariationalProblem(self.bmass, self.Lbres, 
                                               self.bres)
        self.bressolver = LinearVariationalSolver(bresproblem, 
                                             parameters={'ksp_type': 'cg'})


    def GetUResidual(self):
        self.zetasolver.solve()
        self.zadvsolver.solve()
        self.uressolver.solve()

# set parameters
L = 300000.
H = 10000.
nlayers = 10
ncolumns = 300
dt = 12.
nits = 4
alpha = 0.5
Nsq = 1.e-2**2
csq = 300.**2
U = 20.

# set up initial conditions
u0 = Expression(("0.0","0.0"))
v0 = Expression("0.0")
p0 = Expression("0.0")
b0 = Expression("0.01*sin(pi*x[1]/10000.)/(1+pow(x[0]-150000.,2)/pow(5000.,2))")

u0_exp = Expression(("0.0","0.0"))
v0_exp = Expression("0.0")
p0_exp = Expression("0.01*10000*cos(pi*x[1]/10000.)/(1+pow(x[0]-3000000.,2)/pow(100000.,2))")
b0_exp = Expression("0.01*sin(pi*x[1]/10000.)/(1+pow(x[0]-3000000.,2)/pow(100000.,2))")

#dt = 100.
#Model = MeanFlowLinearSlice(L=6000000.,H=10000.,nlayers=50,ncolumns=50,pvertical=1,phorizontal=1,
#                 dt=dt,nits=2,alpha=0.5,Nsq=1.e-4,csq=300.**2,f=1.0e-4,
#                 u0_exp=u0_exp,v0_exp=v0_exp,p0_exp=p0_exp,b0_exp=b0_exp,U=0.)



Model = MeanFlowLinearSlice(L, H, nlayers, ncolumns, 2, 2, dt, nits, alpha, Nsq, csq, 0.0, u0, v0, p0, b0, U)

Model.solve(t=0.0, end=250*dt, Tdump=dt, filename='mean_flow')

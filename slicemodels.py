"""
Tools for creating vertical slice models.
"""
from firedrake import *


class LinearSlice:
    """
    A base class for slice models, with
    residuals for the linear equations.
    """

    def __init__(self,L,H,nlayers,ncolumns,pvertical,phorizontal,
                 dt,nits,alpha,Nsq,csq,f,
                 u0_exp,v0_exp,p0_exp,b0_exp):
        self.L = L
        self.H = H
        dz = H/nlayers
        self.dt = dt
        self.nits = nits
        self.alpha = alpha
        self.Nsq = Nsq
        self.csq = csq
        self.f = f
        
        m = PeriodicIntervalMesh(ncolumns, length=L)
        mesh = ExtrudedMesh(m, layers=nlayers, layer_height=dz)
        self.mesh = mesh

#1D spaces in horizontal
        IUh0 = FiniteElement("CG", "interval", phorizontal)
        IUh1 = FiniteElement("DG", "interval", phorizontal-1)
        self.IUh0 = IUh0

#1D spaces in vertical
        IUv0 = FiniteElement("CG", "interval", pvertical)
        IUv1 = FiniteElement("DG", "interval", pvertical-1)
        self.IUv0 = IUv0

##Setting up the finite element spaces

#Pressure space
        V2_elt = OuterProductElement(IUh1, IUv1)
        V2 = FunctionSpace(mesh, V2_elt)
        self.V2 = V2

#Velocity space
        V1_v = OuterProductElement(IUh1, IUv0)
        V1_h = OuterProductElement(IUh0, IUv1)
        V1_elt = HDiv(V1_v) + HDiv(V1_h)
        V1 = FunctionSpace(mesh,V1_elt)
        self.V1 = V1

#Vertical part of velocity space
        V1v_elt = HDiv(V1_v)
        V1v = FunctionSpace(mesh,V1v_elt)
        self.V1v = V1v

#Vorticity space
        V0_elt = OuterProductElement(self.IUh0,self.IUv0)
        self.V0 = FunctionSpace(self.mesh,V0_elt)

#Temperature space
        Vt = FunctionSpace(mesh,V1_v)
        self.Vt = Vt

#Mixed function space storing all of the velocity field
        W = V1*V2
        self.W = W

#Solution variables
        self.oldvel = Function(W)
        oldu, oldv = self.oldvel.split()
        oldu.project(u0_exp)
        oldv.project(v0_exp)
        self.oldp = Function(V2)
        self.oldb = Function(Vt)
        self.oldb.project(b0_exp)
        self.oldp.project(p0_exp)

#values of u,v,p,b at next timestep
        self.newvel = Function(W)
        self.newp = Function(V2)
        self.newb = Function(Vt,name="Buoyancy")

#iterative increments for newvel, newp, newb
        self.Deltavel = Function(W)
        self.Deltap = Function(V2)
        self.Deltab = Function(Vt)


    def SetupUResidual(self):
        # get parameters
        dt = self.dt
        alpha = self.alpha
        f = self.f
        # get functions
        newu, newv = self.newvel.split()
        oldu, oldv = self.oldvel.split()
        newp = self.newp
        oldp = self.oldp
        newb = self.newb
        oldb = self.oldb
        # get space
        V1 = self.V1
        # set up pbar etc
        pbar = (1-alpha)*oldp + alpha*newp
        bbar = (1-alpha)*oldb + alpha*newb
        vbar = (1-alpha)*oldv + alpha*newv
        # trial and test functions
        du = TrialFunction(V1)
        w = TestFunction(V1)
        self.ures = Function(V1)
        self.umass = inner(w,du)*dx
        self.Lures = (inner(w,newu-oldu) - dt*f*w[0]*vbar
                      - dt*div(w)*pbar - dt*w[1]*bbar)*dx

        uresproblem = LinearVariationalProblem(self.umass, self.Lures, self.ures)
        self.uressolver = LinearVariationalSolver(uresproblem, 
                                             parameters={'ksp_type': 'cg'})

    def GetUResidual(self):
        self.uressolver.solve()

    def SetupVResidual(self):
        # get parameters
        dt = self.dt
        alpha = self.alpha
        f = self.f
        # get functions
        newu, newv = self.newvel.split()
        oldu, oldv = self.oldvel.split()
        # get space
        V2 = self.V2
        # set up ubar etc
        ubar = (1-alpha)*oldu + alpha*newu
        vbar = (1-alpha)*oldv + alpha*newv
        # trial and test functions
        dv = TrialFunction(V2)
        wv = TestFunction(V2)
        self.vres = Function(V2)
        self.vmass = inner(wv,dv)*dx
        self.Lvres = (wv*(newv-oldv) + dt*f*wv*ubar[0])*dx

        vresproblem = LinearVariationalProblem(self.vmass, self.Lvres, self.vres)
        self.vressolver = LinearVariationalSolver(vresproblem, 
                                             parameters={'ksp_type': 'cg'})

    def GetVResidual(self):
        self.vressolver.solve()

    def SetupPResidual(self):
        # get parameters
        dt = self.dt
        alpha = self.alpha
        csq = self.csq
        # get functions
        newu, newv = self.newvel.split()
        oldu, oldv = self.oldvel.split()
        newp = self.newp
        oldp = self.oldp
        # get space
        V2 = self.V2
        # set up ubar etc
        ubar = (1-alpha)*oldu + alpha*newu
        # trial and test functions
        dp = TrialFunction(V2)
        phi = TestFunction(V2)

        self.pmass = phi*dp*dx
        self.Lpres = (
            phi*(newp-oldp + dt*csq*div(ubar))
            )*dx
        self.pres = Function(V2)

        presproblem = LinearVariationalProblem(self.pmass, self.Lpres, 
                                               self.pres)
        self.pressolver = LinearVariationalSolver(presproblem, 
                                             parameters={'ksp_type': 'cg'})

    def GetPResidual(self):
        self.pressolver.solve()


    def SetupBResidual(self):
        # get parameters
        dt = self.dt
        alpha = self.alpha
        Nsq = self.Nsq
        # get functions
        newu, newv = self.newvel.split()
        oldu, oldv = self.oldvel.split()
        newb = self.newb
        oldb = self.oldb
        # get space
        Vt = self.Vt
        # set up ubar etc
        ubar = (1-alpha)*oldu + alpha*newu
        # trial and test functions
        db = TrialFunction(Vt)
        gamma = TestFunction(Vt)

        self.bmass = gamma*db*dx
        self.Lbres = (
            gamma*(newb-oldb + dt*Nsq*ubar[1])
            )*dx
        self.bres = Function(Vt)

        bresproblem = LinearVariationalProblem(self.bmass, self.Lbres, 
                                               self.bres)
        self.bressolver = LinearVariationalSolver(bresproblem, 
                                             parameters={'ksp_type': 'cg'})

    def GetBResidual(self):
        self.bressolver.solve()


    def SetupUSolver(self):
        #THIS IS ALWAYS THE SAME
        # get parameters
        dt = self.dt
        alpha = self.alpha
        csq = self.csq
        Nsq = self.Nsq
        f = self.f
        # get space
        W = self.W
        # trial and test functions
        du, dv = TrialFunctions(W)
        w, wv = TestFunctions(W)
        
#Reduced equation for u,v correction
        dp0 = -alpha*dt*csq*div(du) - self.pres
        db0 = -alpha*dt*Nsq*du[1] -self.bres
        Equ = (
            inner(w,du) - alpha*dt*f*w[0]*dv - alpha*dt*div(w)*dp0
            - alpha*dt*w[1]*db0
            + wv*dv + alpha*dt*f*wv*du[0]
            )*dx
        Au = lhs(Equ)
        Lu = rhs(Equ) - inner(w,self.ures)*dx - inner(wv,self.vres)*dx
#Boundary conditions
        bc1 = [DirichletBC(W[0], Expression(("0." , "0.")), x)
               for x in ["top", "bottom"]]
        uproblem = LinearVariationalProblem(Au,Lu,self.Deltavel,bcs=bc1)
        self.usolver = LinearVariationalSolver(uproblem,
                                               parameters={'ksp_type':'cg'})

    def SetupDpSolver(self):
        #THIS IS ALWAYS THE SAME
        # get parameters
        dt = self.dt
        alpha = self.alpha
        csq = self.csq
        # get space
        V2 = self.V2
        # trial and test functions
        dp = TrialFunction(V2)
        phi = TestFunction(V2)
        Deltavel = self.Deltavel
        Deltau, Deltav = Deltavel.split()
#Reconstruction equation for p correction
        Ldp = phi*(-alpha*dt*csq*div(Deltau) - self.pres)*dx
        pmass = phi*dp*dx
        dpproblem = LinearVariationalProblem(pmass, Ldp, self.Deltap)
        self.dpsolver = LinearVariationalSolver(dpproblem, 
                                                parameters={'ksp_type':'cg'})


    def SetupDbSolver(self):
        #THIS IS ALWAYS THE SAME
        # get parameters
        dt = self.dt
        alpha = self.alpha
        Nsq = self.Nsq
        # get space
        Vt = self.Vt
        # trial and test functions
        db = TrialFunction(Vt)
        gamma = TestFunction(Vt)
        Deltavel = self.Deltavel
        Deltau, Deltav = Deltavel.split()
#Reconstruction equation for b correction
        bmass = gamma*db*dx
        Ldb = gamma*(-alpha*dt*Nsq*Deltau[1] - self.bres)*dx
        dbproblem = LinearVariationalProblem(bmass, Ldb, self.Deltab)
        self.dbsolver = LinearVariationalSolver(dbproblem, 
                                   parameters={'ksp_type':'cg'})


    def SetupPrSolver(self,input,output,outspace):
        # trial and test functions
        tri = TrialFunction(outspace)
        tes = TestFunction(outspace)

        # setup a solver to project input onto outspace
        ax = inner(tes,tri)*dx
        Lx = inner(tes,input)*dx
        prproblem = LinearVariationalProblem(ax, Lx, output)

        prsolver = LinearVariationalSolver(prproblem, parameters={'ksp_type': 'cg'})
        return prsolver


    def dump(self):
        dt = self.dt
        Tdump = self.Tdump
        file = self.file

        if(self.dumpt > Tdump - 0.5*dt):
            self.dumpt -= Tdump
            file << self.oldb


    def solve(self,t,end,Tdump,filename):

        import numpy as np
        
        # get parameters
        dt = self.dt
        nits = self.nits

        # setup residuals
        # Subclasses may redefine these functions
        self.SetupBResidual()
        self.SetupPResidual()
        self.SetupUResidual()
        self.SetupVResidual()

        # setup solvers, these are always the same
        self.SetupUSolver()
        self.SetupDpSolver()
        self.SetupDbSolver()
        
        # set newvel,newp,newb
        self.newvel.assign(self.oldvel)
        self.newp.assign(self.oldp)
        self.newb.assign(self.oldb)

        # open file
        self.file = File(filename+'.pvd')
        self.file << self.oldb

        # time loop
        print "Starting the time loop!"
        dumpt = 0.
        self.dumpt = dumpt
        self.Tdump = Tdump
    
        while (t <= end):
            for its in range(nits):
                self.GetBResidual()
                self.GetPResidual()
                self.GetUResidual()
                self.GetVResidual()

                #This code is always the same
                self.usolver.solve()
                self.dpsolver.solve()
                self.dbsolver.solve()

                    
                self.newvel += self.Deltavel
                self.newp += self.Deltap
                self.newb += self.Deltab
                    
            self.oldvel.assign(self.newvel)
            self.oldp.assign(self.newp)
            self.oldb.assign(self.newb)
                    
            t += dt
            self.dumpt += dt
            print t, end, self.dumpt, self.Tdump

            self.dump()



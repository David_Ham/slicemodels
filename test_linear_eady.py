from linear_eady_model import EadyLinearSlice
from firedrake import *
import inifns

L = 1000000.
H = 10000.
Nsq = 2.5e-05
dbdy = -10./300.*3.0e-06
a = -7.5
Bu = 0.5
dt = 120.
alpha = 0.55

template_s = inifns.template_target_strings()

u0_exp = Expression(("0.0","0.0"))
v0_exp = Expression("0.0")
p0_exp = Expression("0.0")
b0_exp = Expression(template_s,a=a,Nsq=Nsq,Bu=Bu,H=H,L=L)

Model = EadyLinearSlice(L=2*L,H=H,nlayers=61,ncolumns=121,
                        pvertical=1,phorizontal=1,
                        dt=dt,nits=4,alpha=alpha,Nsq=2.5e-5,csq=300.**2,f=1.0e-4,
                        dbdy=dbdy,
                        u0_exp=u0_exp,v0_exp=v0_exp,p0_exp=p0_exp,b0_exp=b0_exp)

Model.solve(t=0.0,end=10*24*60*60.,Tdump=15*60.,filename='test_linear',Sdump='V0')
